

console.log(fetch('https://jsonplaceholder.typicode.com/todos/'));

fetch('https://jsonplaceholder.typicode.com/todos/').then((response)=> response.json()).then((json) => console.log(json));

let result = await fetch('https://jsonplaceholder.typicode.com/todos/');
	
console.log(result);

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/');
	let json = await result.json() 
	var map[]; 
	map = json.map(item => item.title);
	console.log(map);
}
fetchData();


fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST'

}).then((response) => response.json()).then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'PUT'

}).then((response) => response.json()).then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'To do list',
		description: 'This is Updated',
		dateCompleted: 'September 2021',
		userID: 1

	})
}).then((response) => response.json()).then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Completed list',
		date: 'Date changed'
	})
}).then((response) => response.json()).then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE'
}).then((response) => response.json()).then((json) => console.log(json));
